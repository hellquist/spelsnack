---
title: Calendar
---

<iframe src="https://calendar.google.com/calendar/embed?src=3t2d670ejdp2jlbvbhma3ft9bp1g5q8h%40import.calendar.google.com&ctz=Europe%2FStockholm" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

Länk till kalendern ifall du vill lägga till den i din vanliga kalender i dator/mobil osv, utan att gå via Google: https://sesh.fyi/calendar/mLTheGfdPZ2gqGmaipBqWX.ics