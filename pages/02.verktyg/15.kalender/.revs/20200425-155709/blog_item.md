---
title: Kalender
image:
    summary:
        enabled: '1'
        file: kalender.png
summary:
    enabled: '1'
    delimiter: '==='
feed:
    limit: 10
---

Här hittar du en kalender som listar de aktuella händelserna som är relevanta för denna sajt.

===

<iframe src="https://calendar.google.com/calendar/embed?src=3t2d670ejdp2jlbvbhma3ft9bp1g5q8h%40import.calendar.google.com&ctz=Europe%2FStockholm" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

Länk till kalendern ifall du vill lägga till den i din vanliga kalender i dator/mobil osv, utan att gå via Google:<br> https://sesh.fyi/calendar/mLTheGfdPZ2gqGmaipBqWX.ics

Datat till kalendern, alltså det som matar den, är boten Sesh på Discordservern Spelsnack. Det betyder att du lägger till events i kalender genom kommandon till boten. De kommandona ser ut så här:

<pre>
!s create
Create an event using Discord

Exempel:
!s create Apex Legends in 5 hours
!s create Molten Core at 6 PM on Wednesday
!s create CSGO Scrim on 8/21 at 9:30 PM

Create an event using web UI
!s create
!s poll

Create a poll.
!s poll [Favorite color?] blue, red, yellow
!s poll dogs, cats

!s link
Link your calendar

!s list
List events.

!s delete
Delete events.
</pre>