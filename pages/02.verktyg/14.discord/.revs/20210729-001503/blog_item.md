---
title: Discord
media_order: discord_hero.jpg
taxonomy:
    category:
        - verktyg
image:
    summary:
        enabled: '1'
        file: discord_hero.jpg
summary:
    enabled: '1'
    delimiter: '==='
feed:
    limit: 10
---

[editable name="region-0"]Discord är ett ypperligt verktyg för att sköta chat via text, röst och video. Tanken är att vi använder det dels under spelningar på distans, nästan oavsett vilken speltyp eller faktiskt spel vi tänkt att spela. Tanken är också att det kan vara ett ställe där man "hänger" mellan spelningar, och kan få info om när nästa spelning är samt hur man skall förbereda sig för den.[/editable]

===
## Tips & Tricks

* <a href="#mute">Tysta "snackiga" kanaler man inte är intresserad av</a>
* <a href="#sesh">Eventbotten Sesh</a>
* <a href="#generell">Generell Discord information</a>

<a id="mute">&nbsp;</a>

#### Tysta "snackiga" kanaler man inte är intresserad av 

I Discord kan man få rätt mycket meddelanden beroende på hur aktiva de olika kategorierna/kanalerna är. För mycket för en del. Tacksamt nog går det att tysta de saker man inte är intresserad av, och ändå få notiser om de saker man faktiskt ÄR intresserad av.

På våran server har vi delat in servern i "kategorier" som sedan har "kanaler", eller "channels" på engelska. Exempel på kategorier är `Datorspel`, `Regelsystem` (främst RPG i vårat fall) och `Kampanjer`. Exempel på kanaler i tex kategorin `Regelsystem` är `#kult`, `#coriolis` osv. Under kategorin `Datorspel` finns tex `#eve-online` och `#crusader-kings`.

Ifall man är intresserad av `Regelsystem` men INTE av tex `Datorspel` så kan man högerklicka (på dator) på `Datorspel` och välja `Mute Category`. Det betyder "tysta kategori" på engelska. Då får man fortfarande notiser från kategorin `Regelsystem` men inte längre från `Datorspel`.

Ifall man är intresserad av vissa `Regelsystem` men inte av andra kan man högerklicka på de olika kanalerna, tex `#kult` och välja `Mute channel`. Det är engelska för "tysta kanal".

Man kan även välja att få notiser (eller inte) baserat på vad som skrivs i en kanal, precis som i de flesta andra chatprogram ända sedan IRC tiden, men fortfarande är vanligt i tex Slack, Messenger osv. Om någon skriver tex `@all` så går det ut en notis till alla på servern (räknas oftast som dåligt och skall bara göras om det är extremt viktigt). Om man skriver `@channel` går det ut en notis till alla som är med i en kanal. Om man skriver enbart ett användarnamn går en separat notis ut till enbart den spelaren, tex `@CB` kommer att skicka en notis till Bertil.

Det enda som du kan göra fel på här är att stänga av notiser på Discord appen på din telefon. Då får du inga notiser alls och vet således inte ifall någon sagt något av intresse, eller ens ifall någon har "pingat" dig specifikt. Det är därför bättre, för oss som grupp, ifall du har kvar notiser för Discord appen på din telefon, men att du stänger av notiser på kategorier och/eller kanaler du inte är intresserad av.

Skall jag kanske ta screenshots på detta och lägga till i denna förklaring?

<a id="sesh">&nbsp;</a>

#### Eventbotten Sesh 

Vi har en bot på servern som kan visa olika event, göra polls för att hitta bra speldatum osv, men kanske framförallt lista kommande spelningar. Botten "heter" [Sesh](https://sesh.fyi/manual/), och event som skapats i Sesh synkas med [kalendern](https://spelsnack.site/verktyg/kalender), som ni i sin tur kan synka med era telefoner osv ifall ni vill. Då har ni alltid de spelningar som är på gång synkade till era kalendrar/telefoner. Smart va? Rätt mycket bättre funktionalitet än SMS, am I right? Såklart jag är.

För att "trigga" Sesh måste man skriva in ett trigger kommando (trigger "!s") följt av vad man vill göra. Alla kommandon står på länken till manualen.

Ifall man vill se vilka spelningar eller andra event som är "på G" så kan man skriva in följande kommando:

```
!s list
```

Då listas alla spelningar som blivit tillagda i en klickbar lista, som på denna bild:

![Sesh lista med events](sesh_list.jpg)

Där kan man klicka på namnet på den spelning man är intresserad av och då kommer man till den kanal där de detaljerna postats. I just detta fall klickar jag på Coriolisspelningen och tas således till kanalen `#Coriolis` och det aktuella eventet visas.

Där kan jag i sin tur interagera med eventet, genom att trycka på den lilla gröna ikonen som ser ut som ett checkmark/bock. Då bekräftar jag att jag vill och kan vara med på spelningen. Det ser ut så här:

![Sesh event detaljer](sesh_event.jpg)

<a id="generell">&nbsp;</a>

## Generell Discord information

[editable name="region-1"]För våran del så "ägs" den "server" vi använder av Bertil, och heter "Spelsnack". Mathias (T1) är också admin på servern. Vill man ha adminrättigheter är det bara att säga till, men det vore dumt att dela ut admin till alla då risken för fuck-ups ökar ju fler som har det. Dessutom är det svårare att dela saker "privat" (i tex spelningar) ifall alla har admin och kan se allt ändå.

Vem som helst som är på servern kan bjuda in nya deltagare till servern. Jag övervägde att lägga en länk som går direkt till korrekt server här, men noterar att sådana länkar måste användas inom 24 timmar, så det är inte särskilt smidigt sätt att lösa det på. Då vi alla känner typ någon som troligen är på Discord servern redan är nog det enklaste ifall du kollar runt med de du känner om de kan vara vänliga nog att skicka en länk.

Tanken är alltså att vi "hänger" där när det inte är spelningar, men även att vi använder Discord under spelningar, främst för voice/video chat i anslutning till en specifik spelning. Tanken är också att Discord i princip ersätter 2-3 olika grupper på Facebook. Jag vet att Rollspel/Dataspel snacket i princip har flyttats helt till Discord, och även de Brädspelsgrupper jag är med i på FB har i princip tystnat, samtidigt som jag ser lite "nytt-för-mig" folk på Discord-servern. Det är ju kul att kunna bredda både tillgängliga spelare, se vad som händer inom områden man kanske själv inte varit så aktiv inom, samt lära känna nytt folk överlag.

Dock är ju också fördelen med Discord att man kan "mute-a" kanaler man inte är så intresserad av, utan att för den delen behöva "hoppa av" själva servern. En annan fördel är ju att Discord fungerar ypperligt både på desktop och i mobiler, så man behöver inte missa spelningar man är intresserad av.

På Discord kan man också använda sig av så kallade "bots". Vi har försökt hålla antalet bots nere, och just nu har vi 3 stycken med helt olika syften. De är, förutom Sesh som redan nämnts ovan, som följer:

* [BotBoy](https://botboy.snaz.in/commands/?) - allmän informationsbot (trigger "^")
* [Avrae](https://avrae.io/commands) - rollspelsbot med inriktning på DnD (trigger "!!")


Events som läggs till i Sesh kommer dessutom att publiceras ut på en [Google kalender](/verktyg/kalender) så att du kan importera den till en enhet nära dig och alltid känna dig uppdaterad på vad som är på gång.

**Allmän säljblurb:**
Discord is a proprietary freeware VoIP application and digital distribution platform designed for video gaming communities, that specializes in text, image, video and audio communication between users in a chat channel. Discord runs on Windows, macOS, Android, iOS, Linux, and in web browsers. As of 21 July 2019, there are over 250 million unique users of the software.[/editable]