---
title: Discord
media_order: discord_hero.jpg
taxonomy:
    category:
        - verktyg
image:
    summary:
        enabled: '1'
        file: discord_hero.jpg
summary:
    enabled: '1'
    delimiter: '==='
feed:
    limit: 10
---

[editable]Discord är ett verktyg för att sköta chat via text, röst och video. Tanken är att vi använder det dels under spelningar på distans, nästan oavsett vilken speltyp eller faktiskt spel vi tänkt att spela. Tanken är också att det kan vara ett ställe där man "hänger" mellan spelningar, och kan få info om när nästa spelning är samt hur man skall förbereda sig för den.[/editable]

===

[editable]För våran del så "ägs" den "server" vi använder av Bertil, och heter "Spelsnack". Mathias (T1) är också admin på servern. Vill man ha adminrättigheter är det bara att säga till, men det vore dumt att dela ut admin till alla då risken för fuck-ups ökar ju fler som har det. Dessutom är det svårare att dela saker "privat" (i tex spelningar) ifall alla har admin och kan se allt ändå.

Vem som helst som är på servern kan bjuda in nya deltagare till servern. Jag övervägde att lägga en länk som går direkt till korrekt server här, men noterar att sådana länkar måste användas inom 24 timmar, så det är inte särskilt smidigt sätt att lösa det på. Då vi alla känner typ någon som troligen är på Discord servern redan är nog det enklaste ifall du kollar runt med de du känner om de kan vara vänliga nog att skicka en länk.

Tanken är alltså att vi "hänger" där när det inte är spelningar, men även att vi använder Discord under spelningar, främst för voice/video chat i anslutning till en specifik spelning. Tanken är också att Discord i princip ersätter 2-3 olika grupper på Facebook. Jag vet att Rollspel/Dataspel snacket i princip har flyttats helt till Discord, och även de Brädspelsgrupper jag är med i på FB har i princip tystnat, samtidigt som jag ser lite "nytt-för-mig" folk på Discord-servern. Det är ju kul att kunna bredda både tillgängliga spelare, se vad som händer inom områden man kanske själv inte varit så aktiv inom, samt lära känna nytt folk överlag.

Dock är ju också fördelen med Discord att man kan "mute-a" kanaler man inte är så intresserad av, utan att för den delen behöva "hoppa av" själva servern. En annan fördel är ju att Discord fungerar ypperligt både på desktop och i mobiler, så man behöver inte missa spelningar man är intresserad av.

På Discord kan man också använda sig av så kallade "bots". Vi har försökt hålla antalet bots nere, och just nu har vi 3 stycken med helt olika syften. De är som följer:

* [BotBoy](https://botboy.snaz.in/commands/?) - allmän informationsbot (trigger "^")
* [Avrae](https://avrae.io/commands) - rollspelsbot med inriktning på DnD (trigger "!!")
* [Sesh](https://sesh.fyi/manual/) - kalender/event bot där man kan lägga till nya spelningar osv (trigger "!s")

Events som läggs till i Sesh kommer dessutom att publiceras ut på en [Google kalender](/verktyg/kalender) så att du kan importera den till en enhet nära dig och alltid känna dig uppdaterad på vad som är på gång.

**Allmän säljblurb:**
Discord is a proprietary freeware VoIP application and digital distribution platform designed for video gaming communities, that specializes in text, image, video and audio communication between users in a chat channel. Discord runs on Windows, macOS, Android, iOS, Linux, and in web browsers. As of 21 July 2019, there are over 250 million unique users of the software.[/editable]