---
title: Discord
media_order: discord_hero.jpg
image:
    summary:
        enabled: '1'
        file: discord_hero.jpg
summary:
    enabled: '1'
    delimiter: '==='
feed:
    limit: 10
---

Discord är ett verktyg för att sköta chat via text, röst och video. Tanken är att vi använder det dels under spelningar på distans, nästan oavsett vilken speltyp eller faktiskt spel vi tänkt att spela. Tanken är också att det kan vara ett ställe där man "hänger" mellan spelningar, och kan få info om när nästa spelning är samt hur man skall förbereda sig för den.

===

**Allmän säljblurb:**
Discord is a proprietary freeware VoIP application and digital distribution platform designed for video gaming communities, that specializes in text, image, video and audio communication between users in a chat channel. Discord runs on Windows, macOS, Android, iOS, Linux, and in web browsers. As of 21 July 2019, there are over 250 million unique users of the software.