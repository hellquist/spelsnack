---
title: Roll20
---

===
![](roll20-hero.png)

Roll20 är en virtuell spelplattform som är gjord för att spela tabletop RPG's. Som sådan har den en hög med verktyg för att underlätta spelningar på distans.

Roll20 har även inbyggd voice/video funktionalitet. Dock är den inte helt pålitlig (enligt utsagor från Internet, ej självupplevt) så tanken är att vi sköter voice/video i Discord, och använder Roll20 enbart som stöd för att visa kartor, rulla tärning och visa andra "handouts" under spelningarna.

Det finns stöd för en uppsjö med spel. Bäst stöd har Dungeons & Dragons samt Pathfinder. Båda har en massa funktionalitet som inte finns utvecklat för andra spel. Dock finns det stöd, rollformulär och lite andra verktyg för tex Symbaroum, Coriolis, Degenesis, Eclipse Path, Forbidden Lands etc. Det finns också en uppsjö med YouTube videos för tips/tricks för både GM/spelare.

**Från Wikipedia:**

Roll20 is a website consisting of a set of tools for playing tabletop role-playing games, also referred to as a virtual tabletop, which can be used as an aid to playing in person or remotely online. The site was launched in 2012 after a successful Kickstarter campaign. The platform's goal is to provide an authentic tabletop experience that does not try to turn the game into a video game, but instead aids the game master in providing immersive tools. The blank slate nature of the platform makes integrating a multitude of tabletop role-playing games possible.

![](roll20b.png)