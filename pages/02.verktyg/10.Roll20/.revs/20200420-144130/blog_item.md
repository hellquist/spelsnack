---
title: Roll20
---

![](roll20-hero.png)

**Wikipedia:**

Roll20 is a website consisting of a set of tools for playing tabletop role-playing games, also referred to as a virtual tabletop, which can be used as an aid to playing in person or remotely online. The site was launched in 2012 after a successful Kickstarter campaign. The platform's goal is to provide an authentic tabletop experience that does not try to turn the game into a video game, but instead aids the game master in providing immersive tools. The blank slate nature of the platform makes integrating a multitude of tabletop role-playing games possible.