---
title: Bookmarks
process:
    markdown: true
    twig: false
---

===

[ui-tabs position="top-left" active="0" theme="default"]
[ui-tab title="Verktyg"][editable name="region-0"]| Länk | Beskrivning |
| --- | --- |
| Column 1 Item 1 | Column 2 Item 1 |
| [DuckDuckGo](https://duckduckgo.com) | Här kommer en länk från front-end editorn, inloggad som en användare. |[/editable][/ui-tab]
[ui-tab title="Rollspelsrelaterat"][editable name="region-1"]
|  Länk  |  Beskrivning  |
|  :-----          |  :-----          |
|  Column 1 Item 1 |  Column 2 Item 1 |
[/editable][/ui-tab]
[ui-tab title="Brädspelsrelaterat"][editable name="region-2"]
|  Länk  |  Beskrivning  |
|  :-----          |  :-----          |
|  Column 1 Item 1 |  Column 2 Item 1 |
[/editable][/ui-tab]
[ui-tab title="Datorspelsrelaterat"][editable name="region-3"]
|  Länk  |  Beskrivning  |
|  :-----          |  :-----          |
|  Column 1 Item 1 |  Column 2 Item 1 |
[/editable][/ui-tab]
[ui-tab title="Blandat"][editable name="region-4"]
|  Länk  |  Beskrivning  |
|  :-----          |  :-----          |
|  Column 1 Item 1 |  Column 2 Item 1 |
[/editable][/ui-tab]
[/ui-tabs]