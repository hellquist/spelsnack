---
title: Bookmarks
process:
    markdown: true
    twig: false
---

[ui-tabs position="top-left" active="0" theme="default"]
[ui-tab title="Verktyg"][columns count=3][editable name="region-0"]*   This could be a link
*   [DuckDuckGo](https://duckduckgo.com)
*   This could be a third link
*   This could be a link
*   [Google](https://google.com)
*   This could be a link
*   [Altavista](https://altavista.com)
*   This could be a third link
*   [Yahoo](https://yahoo.com)
*   This could be another link
*   This could be a third link[/editable]
[/columns]

[/ui-tab]
[ui-tab title="Rollspelsrelaterat"]
[datatables]
| Länk | Beskrivning |
| --- | --- |
| [DuckDuckGo](https://duckduckgo) | Sökmotorn du bör använda |
| [Google](https://google.com) | Sökmotorn du inte bör använda om du tycker privacy är viktigt, vilket du borde tycka. |
| asdf | qwer |
| zxcv | dsfg |
[/datatables]
[/ui-tab]
[ui-tab title="Brädspelsrelaterat"]
[editable name="region-2"]
* This could be a link too
* And this could be the second link on the second tab
[/editable]
[/ui-tab]
[ui-tab title="Datorspelsrelaterat"]
[editable name="region-3"]
* This could be a link too
* And this could be the second link on the second tab
[/editable]
[/ui-tab]
[/ui-tabs]

[datatables]
|  Column 1 Title  |  Column 2 Title  |
|  :-----          |  :-----          |
[dt-script]
|  Column 1 Item 1 |  Column 2 Item 1 |
|  Column 1 Item 2 |  Column 2 Item 2 |
[/dt-script]
[/datatables]