---
title: Bookmarks
process:
    markdown: true
    twig: false
summary:
    enabled: '1'
    delimiter: '==='
feed:
    limit: 10
---

Här sparar vi bokmärken som kan vara användbara, om inte annat för referens. Det andra syftet är förstås att hjälpa till med att undvika att man måste gå in på varje sida på denna sajt för att hitta en länk till något specifikt.

===

[ui-tabs position="top-left" active="0" theme="default"]
[ui-tab title="Verktyg"][editable name="region-0"]
| Länk | Beskrivning |
| --- | --- |
| Column 1 Item 1 | Column 2 Item 1 |
| [DuckDuckGo](https://duckduckgo.com) | Här kommer en länk från front-end editorn, inloggad som en användare. |[/editable][/ui-tab]
[ui-tab title="Rollspelsrelaterat"][editable name="region-1"]| Länk | Beskrivning |
| --- | --- |
| [Dungeons & Dragons](https://dnd.wizards.com/) | Officiella sajten |
| [Coriolis](https://frialigan.se/sv/games/coriolis/) | Officiella sajten |
| [Symbaroum](https://frialigan.se/sv/games/symbaroum-2/) | Officiella sajten |
| [The Expanse (Green Ronin store)](https://greenroninstore.com/collections/the-expanse-rpg) | Officiella sajten |
| [Degenesis](https://degenesis.com/) | Nytt spel som vi kanske skall testa? |
| [Svärdets sång](https://frialigan.se/sv/games/svardets-sang/) / Forbidden Lands | Nytt (för oss) spel som vi borde testa! |[/editable][/ui-tab]
[ui-tab title="Brädspelsrelaterat"][editable name="region-2"]
|  Länk  |  Beskrivning  |
|  :-----          |  :-----          |
|  Column 1 Item 1 |  Column 2 Item 1 |
[/editable][/ui-tab]
[ui-tab title="Datorspelsrelaterat"][editable name="region-3"]
|  Länk  |  Beskrivning  |
|  :-----          |  :-----          |
|  Column 1 Item 1 |  Column 2 Item 1 |
[/editable][/ui-tab]
[ui-tab title="Blandat"][editable name="region-4"]
|  Länk  |  Beskrivning  |
|  :-----          |  :-----          |
|  Column 1 Item 1 |  Column 2 Item 1 |
[/editable][/ui-tab]
[/ui-tabs]