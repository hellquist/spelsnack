---
title: Bookmarks
process:
    markdown: true
    twig: true
---

[datatables]

|  Column 1 Title  |  Column 2 Title  |
|  :-----          |  :-----          |
|  Column 1 Item 1 |  Column 2 Item 1 |
|  Column 1 Item 2 |  Column 2 Item 2 |
|  Column 1 Item 3 |  Column 2 Item 3 |
|  Column 1 Item 4 |  Column 2 Item 4 |

[/datatables]