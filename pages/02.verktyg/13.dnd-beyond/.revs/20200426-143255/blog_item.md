---
title: 'DnD Beyond'
feed:
    limit: 10
---

[editable name="region-0"]

**Wikipedia:**

D&D Beyond (DDB) is the official digital toolset and game companion for Dungeons & Dragons fifth edition. DDB hosts online versions of the official Dungeons & Dragons fifth edition books, including rulebooks, adventures, and other supplements; it also provides digital tools like a character builder and digital character sheet, monster and spell listings that can be sorted and filtered, an encounter builder, and an interactive overlay Twitch Extension. In addition to official D&D content, it also provides the ability to create and add custom homebrew content.

D&D Beyond also publishes regular original video, stream, and article content, including interviews with Dungeons & Dragons staff, content previews and tie-ins, and weekly development updates.

[/editable]