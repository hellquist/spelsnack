---
title: 'Fantasy Grounds'
taxonomy:
    category:
        - verktyg
image:
    summary:
        enabled: '1'
        file: fantasy_grounds.jpg
summary:
    enabled: '1'
    delimiter: '==='
feed:
    limit: 10
---

**Wikipedia:**

Fantasy Grounds is a virtual tabletop application, which contains a set of tools to assist players of tabletop role-playing games playing either in person or remotely.