---
title: 'Fantasy Grounds'
---

**Wikipedia:**

Fantasy Grounds is a virtual tabletop application, which contains a set of tools to assist players of tabletop role-playing games playing either in person or remotely.