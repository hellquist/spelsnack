---
title: 'Fantasy Grounds'
taxonomy:
    category:
        - verktyg
image:
    summary:
        enabled: '1'
        file: fantasy_grounds.jpg
summary:
    enabled: '1'
    delimiter: '==='
feed:
    limit: 10
---

Fantasy Grounds (FG) är en virtuell spelplattform som är gjord för att spela tabletop RPG's. Som sådan har den en hög med verktyg för att underlätta spelningar på distans.

Precis som Roll20, som är den närmaste konkurrenten, så hanterar Fantasy Grounds en uppsjö med spel, via moduler som man köper till. Bäst stöd finns för D&D samt Pathfinder, men det finns även stöd för en hög med andra spel.

===

Till skillnad från Roll20 så körs FG som en installerad applikation. Det betyder förstås att man få ett plattformsberoende på ett annat sätt än tex Roll20 som körs i en webbläsare, men komplexiteten slutar inte där: den "gamla" versionen av FG, "Classic" (FGC) fungerar enbart på äldre Mac datorer. Då den är 32-bitars går det inte ens att installera på Macar som kör uppdaterade OS. FG kör dock en "öppen beta" just nu på nästa version av Fantasy Grounds, som har tilläggsnamnet "Unity", dvs Fantasy Grounds Unity (FGU). Den kan också köras på Windows och Mac, men denna funkar **bara** på nyare Macar, inte på äldre version av macOS. Tyvärr är de två olika versionerna inte kompatibla med varandra, vilket betyder att man bör ha full koll på alla spelares utrustning innan man påbörjar spelningen, annars finns det risk att 1-2 personer kanske inte kan vara med trots allt.

Detta leder ju till en del andra avvägningar man måste göra: man *vet* att utvecklingen på FGC kommer att avslutas. De som har FGC idag får dessutom en rabatt på att "uppdatera" till FGU, men det är som sagt bara intressant ifall man vet att hela spelgruppen klarar av de kraven. I vårat fall är det inte så. Just nu så är nätverksuppkopplingen för FGU inte heller särskilt stabil, och det är mycket stabilare uppkoppling i "gamla" FGC.

Det kan även vara värt att notera prismodellen, som är lite annorlunda än tex Roll20:

Man kan låta spelledaren stå för kalaset. Då måste denne betala $149 men kan hålla spelningar för gratis-klienter. Alternativet är att alla i hela spelgruppen betalar $39 vardera, för att kunna spela tillsammans. Däremot så kan någon som betalat $39 inte hålla spelningar för gratis-klienter. 

När man väl har klienten installerad och man har en spelning laddad med tillhörande regel-modul så gör dock FG en hel del saker åt en, som att rulla tärning, visa kartor osv. Har man FGU (dvs nya varianten) så finns det även "dynamic lighting" samt "field-of-view" och "fog-of-war". Inget av de sakerna finns i den gamla varianten FGC.

**Wikipedia:**

Fantasy Grounds is a virtual tabletop application, which contains a set of tools to assist players of tabletop role-playing games playing either in person or remotely.