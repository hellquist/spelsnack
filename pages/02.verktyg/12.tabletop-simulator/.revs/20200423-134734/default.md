---
title: 'Tabletop Simulator'
---

Tabletop 


**Wikipedia:**

Tabletop Simulator is an independent video game that allows players to play and create tabletop games in a multiplayer physics sandbox. Developed by Berserk Games as their first title, after a successful crowdfunding campaign in February 2014 the game was released in June of the following year.