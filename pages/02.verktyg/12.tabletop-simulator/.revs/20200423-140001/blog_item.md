---
title: 'Tabletop Simulator'
taxonomy:
    category:
        - verktyg
image:
    summary:
        enabled: '1'
        file: tabletop_simulator.jpg
feed:
    limit: 10
---

Tabletop Simulator (TTS) är en simulator som har ett lite annorlunda koncept jämfört med tex Roll20 eller Fantasy Grounds, som båda är helt inriktade på att vara hjälpmedel för traditionella rollspel. TTS å andra sidan kan simulera "bordet" där du spelar, och vad du har på bordet kan lika gärna vara ett enkelt brädspel som Fia med Knuff och Monopol, som det kan vara ett "riktigt" brädspel som tex Scythe, hela vägen upp till att vara ett rollspelsbord där man kan ställa figurer, monster och miljöer på det fysiska, fast då digitala, bordet.

Vad gäller just brädspel så finns det en uppsjö av saker och Downloadable Content (DLC) som man kan köpa till eller i vissa fall ladda ner gratis.

Det finns även tillbehör för traditionella rollspel, i synnerhet för D&D.

Då jag utforskat det relativt lätt (än så länge) så verkar det rätt kompetent. Som en avancerad version av whiteboarden vi brukar ha mitt på rollspelsbordet för att rita på. Nackdelen jämfört med whiteboard är väl att det tar lite längre tid att "ställa upp" allt. Jag har heller inte, hittills, hittat någon funktion för "scener", något som är väldigt behändigt ifall man spelar rollspel och spelgruppen kanske kommer att ha händelser på olika kartor under en spelsession (tex "värdshus", "vakttorn", "skogen" och "grotta"), något som är väldigt enkelt att byta mellan i tex Roll20 och Fantasy Grounds.


**Wikipedia:**

Tabletop Simulator is an independent video game that allows players to play and create tabletop games in a multiplayer physics sandbox. Developed by Berserk Games as their first title, after a successful crowdfunding campaign in February 2014 the game was released in June of the following year.