---
title: 'Divinity Original Sin 2'
---

**Wikipedia:**

Divinity: Original Sin II is a role-playing video game developed and published by Larian Studios. The sequel to 2014's Divinity: Original Sin, it was released for Microsoft Windows in September 2017, for PlayStation 4 and Xbox One in August 2018, for macOS in January 2019, and Nintendo Switch in September 2019.

The game received universal acclaim, with many critics praising its complexity and interactivity, considering it to be one of the best role-playing games of all time. It was also a commercial success, selling over a million copies in two months.