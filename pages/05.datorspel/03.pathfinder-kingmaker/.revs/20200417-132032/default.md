---
title: 'Pathfinder Kingmaker'
---

**Wikipedia:**

Pathfinder: Kingmaker is an isometric role-playing game developed by Russian studio Owlcat Games and published by Deep Silver, based on Paizo Publishing's Pathfinder franchise. Announced through a Kickstarter campaign in 2017, the game was released for Microsoft Windows, macOS, and Linux on 25 September 2018.