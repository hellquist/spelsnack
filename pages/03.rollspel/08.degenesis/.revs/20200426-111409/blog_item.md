---
title: Degenesis
taxonomy:
    category:
        - rollspel
feed:
    limit: 10
---

[editable]Eshaton. That’s what they call the end of the world. The day when fire rained from the Heavens, burning the land, scorching the people. The planet trembled, heaving in pain like a feverish person in agony. And though Earth endured,
it was forever changed.

When Eshaton fell and the Bygone people perished,they took with them ten thousand years of culture. The survivors scavenged and fought for food and clean water. Empty-eyed, they stared at the rotting vehicles of their ancestors, wandering aimlessly through the ruins of a once great civilization. A civilization they had shed long ago, casually as a matter of fact, like a snake shedding its skin. Free of morals and ethics, as naïvely as children they looked upon their devastated world, upon landscapes tortured by the elements, upon toxic restricted areas...They only knew that they must hold their ground against this new environment or succumb to it.

Time passed. The smoke above the great craters blew away, and the people had once more erected a cultural framework around their lives. It was still shaky, and the nails were few and far between. Now and then, a civilization crashed down with a din – but the building blocks were reused. Botch jobs, but a new start after years of decline.

The year is now 2595. Europe is divided into several warring Cultures. The people of Borca cling to the Bygone’s relics. Frankers thrash around in the Aberrants’ pheromone net. Purgare is a land of half burnt and half fertile plains, but all together shattered by feuds against the Psychokinetics. The Pollen people wander from oasis to Fractal Forest before even the last green area is devoured by the Sepsis and the biokinetic plague. Hybrispania suffers from a decades-long struggle for liberation and a growing time anomaly. And beyond the Mediterranean, Africa shines in Gold and Lapis Lazuli struggles for its existence against a strange, aggressive vegetation.

Seven Cultures, thirteen cults, countless clans. Which peoples, philosophies, or faiths will prevail? Those that conjure up past glory? Or those that have erected a brave new world upon the ruins of human arrogance?

In the craters’ shadows, something is stirring. Is there a future for Mankind at all?

Degenesis is about hope and despair. It is about people and the conflicting priorities of human civilization, daring to ask how far our race has truly come since we climbed down from the trees. The world of Degenesis is like a ruined Garden of Eden, containing the secrets and spoils of both good and evil, of ignorance and enlightenment, of barbarity and virtue.

As a role playing game, Degenesis presents this world to players who portray characters (“PCs”) faced with this inhospitable future. They’ll need to make a stand that will influence the path of their lives and the fate of those
around them, if not the world and civilization at large – for better or for worse. It’s up to them.[/editable]