---
title: 'The Expanse'
---

**VENTURE INTO THE EXPANSE!**

The Expanse RPG CoverThe Expanse Roleplaying Game brings James S.A. Corey’s award-winning series of science fiction novels to the tabletop. Using the Adventure Game Engine (AGE) rules found in such Green Ronin RPGs as Fantasy AGE, Blue Rose, and Modern AGE, The Expanse takes players to a far-future solar system where humanity is divided: Martians, Belters, and the people of old Earth struggle for political power and resources, but even older, alien, forces are stirring in the universe, and human history is about to take an unexpected new turn. The Expanse applies the fast-playing and action-based AGE rules to spaceships, solar colonies, and adventure and intrigue in the far-future, where the actions of the characters may change the course of history!

**Expanse Features**

The Expanse is based on the exciting new Modern AGE rules-set, and includes many of its features, such as customized character building using Backgrounds and Professions, Drives for character engagement, and an abstract resources system. It also makes use of the Modern AGE approach to action, exploration, and social encounters, complete with stunts and systems for all of them.

The following are some of the unique features of The Expanse in comparison to other AGE System games:

* Fortune: Rather than Health, characters have a Fortune score that measures lucky near-misses, close scrapes, and trivial hits before the character takes serious harm. Fortune is also useful for modifying die rolls and offering players some narrative control but, watch out! Spend too much of it and your luck could run out when you get caught in a crossfire!
* Conditions: In addition to a running Fortune total, characters use various conditions to measure things like injuries and fatigue as well as tactical challenges like hindered movement or sensory abilities.
* Interludes: The interludes in between encounters are given their own treatment, allowing players to make use of their “down-time” (including long hauls between destinations in the System) to recover, do maintenance, build their connections with others, or pursue their own projects.
Spaceships: The Expanse RPG features a system to model and create spaceships and its own system for space combat, including the assault on Thoth Station as an example of the system in action!
* The Churn: The Expanse also offers something for the Game Master with the Churn: A ticking counter that measures the crew’s progress through a story and just when things are going to suddenly go sideways and become even more complicated!