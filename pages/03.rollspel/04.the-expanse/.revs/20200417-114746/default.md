---
title: 'The Expanse'
---

The Expanse Roleplaying Game brings James S.A. Coreys award-winning series of science fiction novels to the tabletop. Using the Adventure Game Engine (AGE) rules that power Green Ronins Fantasy AGE, Blue Rose, and Modern AGE RPGs, The Expanse takes players to a far-future solar system where humanity is divided: Martians, Belters, and the people of old Earth struggle for political power and resources, but older, alien, forces are stirring in the universe, and human history is about to take an unexpected new turn.

The Expanse RPG applies the fast-playing, stunt-powered AGE system to spaceships, solar colonies, adventure, and intrigue in the far-future, where the actions of the characters may change the course of history!