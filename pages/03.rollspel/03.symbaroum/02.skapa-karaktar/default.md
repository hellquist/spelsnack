---
title: 'Skapa karaktär'
taxonomy:
    category:
        - symbaroum
---

[editable name="region-0"]Här kan man ha instruktioner för hur man gör en karaktär.
Troligen är det bra med en pdf eller något för att ladda ner/skriva ut också.[/editable]