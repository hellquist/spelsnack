---
title: Symbaroum
media_order: symbaroum-hero.jpg
taxonomy:
    category:
        - rollspel
image:
    summary:
        enabled: '1'
        file: symbaroum-hero.jpg
feed:
    limit: 10
content:
    items: '@self.modular'
    order:
        by: date
        dir: desc
---

[editable name="region-0"]På denna sidtyp kan man kanske lista allmän spelinfo, vem som är spelledare för detta hos oss, och när nästa spelning är planerad. Lite länkar till online-resurser och lite coola bilder från spelet skadar ju heller inte.

Sedan kan man tänka sig att det finns underlänkar till planerade/existerande/pågående kampanjer/äventyr, där man även kan hitta introduktionsmaterial för just den kampanjen. Helst skall man även kunna ha en "logg" över vart spelarna är just nu också, och i grova drag vad som hände på senaste spelningen, så att det blir enklare att plocka upp igen för existerande spelare, men även så att det blir enklare för nya spelare att komma in i matchen och bli hyggligt uppdaterade på vad som hänt tidigare. Testar att editera detta från front-end editorn igen, fast nu med ett större stycke.

**Allmän spel-info blurb:**

Fantasyrollspelet Symbaroum utspelar sig i och omkring det vidsträckta skogsbältet Davokar; en uråldrig skog, en skog med ett mörkt förflutet och ett ännu mörkare hjärta. Det är en skog som inte får störas och vars spetsörade väktare tillgriper våld mot alla som kränker de fridstraktat som stämplats av många, sedan länge döda, människofurstars sigill. Traktaten har inte hotats på århundraden. Inte på allvar. Förrän nu.

För två decennier sedan ledde en ung drottning sitt desperata folk norrut, på flykt undan hemlandets krigshärjade och svartkonstsjuka marker. Hon slog sig till ro på den bördiga slätten söder om skogarna efter att ha fördrivit eller pacificerat de barbarfurstar som tidigare härskat där. Därefter prisade hon sin Gud, deklarerade rikets pånyttfödelse och beordrade uppförandet av ett högsäte, praktfullt nog att göra hennes nya förlovade marker rättvisa.

Kort efter ankomsten gjorde drottningens folk sina första intrång i Davokar, på jakt efter naturresurser och sökande efter rikedomar bland områdets många välbevarade ruiner. De lokala barbarstammarna och deras häxor avrådde strängt från sådana tilltag, inte enbart med hänvisning till skarpskjutande väktare, glupska vilddjur och kringströvande ärketroll. Med envishet och övertygelse hävdade de också att endast trädens rötter och ett tunt lager mylla skyddar skogens invånare från den ondska som slumrar i lövsalarnas djup - styggelser och ovärldsväsen som för länge sedan krossade Symbaroum, barbarfolkens fornstora högkultur. Men drottning Korinthia och hennes folk lyssnade inte och nu rister ondskan oroligt i sömnen.

Skogen håller på att vakna.

Rollpersonerna kan anta många skiftande uppgifter i den konfliktfyllda miljön. De kan bidra till drottningens grandiosa civilisationsbygge, alliera sig med barbarfolken i strävan att förstå och bevara skogarnas hemlighet, eller utmana områdets urgamla makter för att nå sina personliga mål. Hur de än väljer utlovas nervkittlande strövtåg genom skogsklädda ruiner, nedstiganden i avgrundsdjupa slukhål, färder längs nattsvarta vattenstråk och konfrontationer med barbarhövdingar, ärketroll och fasansväckande ovärldsväsen - allt detta och mer, i en värld där äventyret är allt![/editable]