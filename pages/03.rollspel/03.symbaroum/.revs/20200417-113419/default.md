---
title: Symbaroum
---

Fantasyrollspelet Symbaroum utspelar sig i och omkring det vidsträckta skogsbältet Davokar; en uråldrig skog, en skog med ett mörkt förflutet och ett ännu mörkare hjärta. Det är en skog som inte får störas och vars spetsörade väktare tillgriper våld mot alla som kränker de fridstraktat som stämplats av många, sedan länge döda, människofurstars sigill. Traktaten har inte hotats på århundraden. Inte på allvar. Förrän nu.

För två decennier sedan ledde en ung drottning sitt desperata folk norrut, på flykt undan hemlandets krigshärjade och svartkonstsjuka marker. Hon slog sig till ro på den bördiga slätten söder om skogarna efter att ha fördrivit eller pacificerat de barbarfurstar som tidigare härskat där. Därefter prisade hon sin Gud, deklarerade rikets pånyttfödelse och beordrade uppförandet av ett högsäte, praktfullt nog att göra hennes nya förlovade marker rättvisa.

Kort efter ankomsten gjorde drottningens folk sina första intrång i Davokar, på jakt efter naturresurser och sökande efter rikedomar bland områdets många välbevarade ruiner. De lokala barbarstammarna och deras häxor avrådde strängt från sådana tilltag, inte enbart med hänvisning till skarpskjutande väktare, glupska vilddjur och kringströvande ärketroll. Med envishet och övertygelse hävdade de också att endast trädens rötter och ett tunt lager mylla skyddar skogens invånare från den ondska som slumrar i lövsalarnas djup - styggelser och ovärldsväsen som för länge sedan krossade Symbaroum, barbarfolkens fornstora högkultur. Men drottning Korinthia och hennes folk lyssnade inte och nu rister ondskan oroligt i sömnen.

Skogen håller på att vakna.

Rollpersonerna kan anta många skiftande uppgifter i den konfliktfyllda miljön. De kan bidra till drottningens grandiosa civilisationsbygge, alliera sig med barbarfolken i strävan att förstå och bevara skogarnas hemlighet, eller utmana områdets urgamla makter för att nå sina personliga mål. Hur de än väljer utlovas nervkittlande strövtåg genom skogsklädda ruiner, nedstiganden i avgrundsdjupa slukhål, färder längs nattsvarta vattenstråk och konfrontationer med barbarhövdingar, ärketroll och fasansväckande ovärldsväsen - allt detta och mer, i en värld där äventyret är allt!