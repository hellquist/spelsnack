---
title: Logg
taxonomy:
    category:
        - symbaroum
---

Här blir det som en (b)logg över spelgruppens senaste bravader.
Den kommer att tjäna som historisk dokumentation över fram/mot-gångar.