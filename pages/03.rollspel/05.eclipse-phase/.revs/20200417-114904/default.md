---
title: 'Eclipse Phase'
---

Your mind is software - program it!

Your body is a shell - change it!

Death is a disease - cure it!

Extinction is approaching - fight it!

In Eclipse Phase, the award-winning, post-apocalyptic roleplaying game of transhuman conspiracy and horror, technology allows the re-shaping of bodies and minds, but also creates opportunities for oppression and puts the capability for mass destruction in the hands of everyone. Other threats lurk in the devastated habitats of the Fall, dangers both familiar and alien. In this harsh setting, players participate in a cross-faction conspiracy called Firewall that seeks to protect transhumanity from threats both internal and external. Revised and redesigned for the next evolution, Eclipse Phase, Second Edition introduces exciting, newly enhanced features including a faster character creation system, streamlined resleeving, updated gameplay, and pre-fabbed character teams.