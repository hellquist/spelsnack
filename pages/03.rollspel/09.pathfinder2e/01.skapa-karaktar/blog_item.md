---
title: 'Skapa karaktär'
feed:
    limit: 10
---

Nu gör vi om allt, fast på ett nytt sätt. :)

Dessa instruktioner är **inte** fullständiga. Däremot är filen nedan möjlig att ladda ner redan nu. Den innehåller enbart 7 feats som kommer från böckerna som GM använder sig utav. Jag skall skriva ihop lite mera fullständiga instruktioner senare ikväll.

# Instruktioner - Pathbuilder 2e

Pathbuilder2e.com instruktioner
(Förutsätter du betalat för tjänsten)

## Generella inställningar

!!!! OBS!! Jag kan tipsa om att ifall du sparar din karaktär på Google Drive istället för "Local" så kommer du att kunna öppna din karaktär på vilken dator/platta som helst som har tillräckligt stor skärmbredd (dvs inte telefoner).

* Gå till **[https://pathbuilder2e.com](https://pathbuilder2e.com)**
* OBS!! --> Logga in och/eller betala $6 för alla features <-- OBS!!
* Ladda ner **[202311092147_SmokeAndSail.json](202311092147_SmokeAndSail.json)** **<-- SENAST UPPDATERAD 9/11**
* Menu -> Custom Pack -> Import Custom Pack
* Notera även att ifall du har den gamla custom pack filen installerad så kan du deleta den här.

### Uppdateringar

Filen för **CustomPack** (ovan) uppdateras med jämna mellanrum, oftast ett par gånger/vecka just nu. Det är dock lätt att uppdatera den, enbart. Bara att ladda ner den senaste versionen och sedan göra de stegen, enbart, av instruktionerna ovan, dvs:

* Ladda ner senaste filen
* Menu -> Custom Pack -> Import Custom Pack

Uppdateringarna är "icke-destruktiva" så skall inte påverka något annat, och de handlar mest om tillägg till saker som kommer att komma, eller att någon feat har flyttats från en lista till en annan.

## Level 1

* Menu -> Character Options
  * Se till att alla böcker är "på" under "Manage Available Rulebooks"
  * Tryck på "Finished"

* Menu -> Character Options
  * Rulla ner till Advanced Options
  * Klicka i kryssrutan för "Allow Gamesmastery Guide Dual Classing"
  * Klicka även i kryssrutan för "Allow Gamesmastery Guide Free Archetype"

Gör din karaktär genom att välja grundklass och secondary class, background osv.


## Level 2

* Under Free Archetype skall du välja "Pirate Dedication".

En sak som är viktig att komma ihåg nu är följande:

* När man väljer Pirate Dedication får man automatiskt "Lore: Sailing". Det är däremot inte vad vi skall använda, då vi kör med expanderade 3:e parts regler, som ersätter Lore: Sailing med mera detaljerade skills. Det går däremot inte att ta bort "Lore: Sailing" från skills listan.
* Således får Lore: Sailing "representera" din mera detaljerade kunskap. Det du har i värde på Lore Sailing kommer att vara det värde du har på skillen som du ersätter den med. Du bör dock skriva ner (tex under fliken Details där man kan skriva anteckningar) vilken specialskill just du har.

De skills du kan välja att ersätta Lore: Sailing med är följande:

* **Navigation (Wis)**
  * Denna är även ett grundkrav för en hög(7) med Feats
  * Kräver att man även skapar en egen "Custom Buff" för att korrigera sitt värde med skillnaden mellan Wis och Int
* **Andra Lore Skills (Int)**
  * **Naval Military** - Knowledge of ship armaments and established traditional navies of nations like Cheliax.
  * **Notable Pirates** - Knowledge of current Pirates and Free Captains
  * **Oceanography** - Knowledge of the ocean's biology and the relationships between ocean life and their abiotic world. This is a specialized version of Nature, making ocean checks involving Knowledge Nature easier by reducing the DC by anything from -2 to -4.
  * **Pirate History** - Knowledge of non-current Pirates and Free Captains and old sea tales
  * **Ships** - Know the difference between sailing ships and their strengths and weaknesses.

En annan sak som är utav vikt är att vi måste tillsätta lite olika roller på skeppet. Dessa roller har uppgifter, både under strid men även under allmänt "båtande", när vi åker runt. Vilken roll du skall ha kan påverka vilken skill du bör ersätta Lore: Sailing med.

**OBS!!** Det finns en hög med viktiga roller på skeppet som **inte** har så mycket med seglingen att göra. De är fortfarande viktiga, och vi behöver namn på de också. Exempel är kock, Quartermaster, Doctor osv. De täcks dock inte av följande lista. Tanken är att ni som har sådana roller troligen även behöver ha "föra skeppet under strid"-roller också.

Följande roller behöver besättas på skeppet när vi är ute och seglar, och samtliga av de följande rollerna har rollspecifika tasks de kan genomföra under tex sjöslag osv:

| Roll                 | Beskrivning                                                                                                                                                                                                                                                                                                                                                                         |   |
|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|
| **Boatswain**            | The boatswain (or bosun) works the ship's deck as the foreman of the unlicensed deck crew. She is in charge of the Riggers and directs their actions, and also serves as the First Mate of the ship.                                                                                                                                                                                |   |
| **Captain**              | The Captain’s role in combat is about encouraging and brow-beating the crew. Whether the Captain calls all the shots or just is a loud mouth is entirely up to the PCs!                                                                                                                                                                                                             |   |
| **Carpenter**            | The carpenter is a fixer and a problem solver. Naval combat is deadly, and even the most onesided combat can result in significant damage to the victorious ship. Patching holes, fixing sails and gauging the damage to the enemy ship are all crucial functions the Carpenter performs.                                                                                           |   |
| **Helmsman**             | You are at the wheel when the waves are crashing and cannonballs are flying. It is your responsibility to position the ship ready to attack, and defend incoming fire.                                                                                                                                                                                                              |   |
| **Master at Arms**       | Being a master at arms is not for the faint of heart. They assist characters in all other roles on the ship, making the other characters’ jobs easier and allowing them to accomplish more in their primary duties usually through aggressive encouragement. They are also always ready to take the fight to the enemy and are often the first across the gap in a boarding action. |   |
| **Master Gunner**        | You operate your ship’s various artillery weapons, using them to neutralize or sink enemy vessels. Note that other pirates can fire artillery as well as there is a Common (swabbie) action for this. However, Master Gunners can perform these activities more quickly and more efficiently.                                                                                       |   |
| **Navigator**            | The Navigator plots the course of the ship and looks out for any dangers that might be lurking just below the surface of the waves.                                                                                                                                                                                                                                                 |   |
| **Swabbies and Riggers** | Common crew actions are often the lifeblood of the ship, and keep it running smoothly and efficiently.                                                                                                                                                                                                                                                                              |   |
