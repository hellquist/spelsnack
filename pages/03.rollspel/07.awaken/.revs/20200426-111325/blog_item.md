---
title: Awaken
taxonomy:
    category:
        - rollspel
---

"Awaken" is an indie tabletop roleplaying game, set in a fantasy world divided by Great Cities of the Alliance, ruled by ruthless politicians and threatened by creatures lurking in the dark underground. During the recent turbulent history, a new species arises; Vasalli. More than humans, these powerful beings are trying to find their place in the society while searching for their origins.

The game is set in a fantasy world filled with wonders, rich and diverse, filled with civilisational achievements and remnants of the mythical past. Explore the various domains of the Great Cities and their surroundings, together with the secrets they bear.

Meet the lurking threats and tremendous forces head on as powerful Vasalli, blessed with impressive Gifts and abilities which allow you for different styles of play.
The setting of the game allows a wide breadth of story possibilities, limited only by the creativity and wild and imaginative ideas of the Narrator.

The roleplaying and narration system are designed to immerse the players in their alter egos and let them cooperate in narration. The rules are light and clear, suitable for experienced and new players alike. The character development is skill-based, depending on how you use their abilities.
Cinematic and vivid combat system, designed to maximize fun and bypass downtime. Creative and unhampered, yet amazingly simple and smooth.
Only the core rulebook and your six-sided dice are needed to jump right into the story full of excitement!

In the game, the players assume the roles of powerful beings named Vasalli. Born as ordinary humans, at some point their lives change as they go through the Awakening. Sometimes it begins slowly; some people evolve and gradually show more potential. Other times the change comes violently and rapidly, brought by an unfortunate event or shock. Awakened Vasalli have extraordinary capabilities and powers, which earn them social prestige. The world they inhabit is divided amongst the Great Cities which formed a loose Alliance. This fragile coalition is eroding on a daily basis, threatened by permanent political struggles and dangers roaming in the wilds. Making this even worse is a creeping terror from underground, the Vargans, set on vengeance against humanity. Meanwhile, the last guardians of humanity, the Colossi, are slowly fading in number.

The system is designed to emphasize narration and story flow, forgoing unnecessary rolls in favor of player creativity. When needed, the common six sided dice are rolled, their number depending on the degree of the skill development. The characters progress with the story; the players develop their skills by applying them and can see their alter egos growing through the game.