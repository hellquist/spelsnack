---
title: Awaken
---

Awaken as a dark fantasy, RPG, with an extraordinary tabletop RPG design, puts the player in the role of powerful humans, called Vasilli who are gifted with unique abilities. The Mediterranean and Slavic mythology have inspired this game because the creators come from the Slavic region. Experience, play and enjoy are the three major aspects that Awaken has set the focus on.