---
title: 'Forbidden Lands'
---

A.k.a Svärdets sång på svenska.

Svärdets sång tar ett nytt grepp om klassisk fantasy. I detta rollspel är ni inte hjältar som skickas på uppdrag av andra - i stället är ni äventyrare och skattletare som går er egen väg i ett fördömt land. I spelet kan ni utforska Det glömda landet, genomsöka uråldriga ruiner, kämpa mot drakar och demoner, och - om ni överlever tillräckligt länge - bygga ert eget fäste i en grym och härjad värld.