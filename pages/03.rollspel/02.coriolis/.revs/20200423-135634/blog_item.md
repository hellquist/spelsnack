---
title: Coriolis
taxonomy:
    category:
        - rollspel
feed:
    limit: 10
---

Den tredje horisonten. En avlägsen och isolerad del av galaxen. Ett område härjat av motsättningar, krig och förödelse. Den plats som människorna ombord på rymdfarkosten Zenit mötte när de efter flera århundranden av resa nådde sitt slutmål. Under deras färd hade människan hittat andra vägar genom rymden. Dunkla rykten talar om emissarierna: sändebud från en människofrämmande civilisation i djupet av gasjätten Xene. Många betraktar dem som gudarnas budbärare, förmedlare av löften om en ny begynnelse. Andra ser dem som domedagens förelöpare, utsända av det mörker som hotar mellan världarna.