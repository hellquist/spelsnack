<?php return array(
    'root' => array(
        'name' => 'gantry/grav-compat',
        'pretty_version' => '5.5.17',
        'version' => '5.5.17.0',
        'reference' => 'f8253982d1adf7200218cbb49f4364db047b235c',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'gantry/grav-compat' => array(
            'pretty_version' => '5.5.17',
            'version' => '5.5.17.0',
            'reference' => 'f8253982d1adf7200218cbb49f4364db047b235c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'scssphp/scssphp' => array(
            'pretty_version' => 'v1.11.1',
            'version' => '1.11.1.0',
            'reference' => 'ace2503684bab0dcc817d7614c8a54b865122414',
            'type' => 'library',
            'install_path' => __DIR__ . '/../scssphp/scssphp',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
