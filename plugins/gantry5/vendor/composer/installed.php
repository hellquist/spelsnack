<?php return array(
    'root' => array(
        'name' => 'rockettheme/gantry5',
        'pretty_version' => '5.5.17',
        'version' => '5.5.17.0',
        'reference' => 'f8253982d1adf7200218cbb49f4364db047b235c',
        'type' => 'grav-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'leafo/scssphp' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '0a06cb2188fcda02f78c9c972f8df1c4c962971e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../leafo/scssphp',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'dev_requirement' => false,
        ),
        'rockettheme/gantry5' => array(
            'pretty_version' => '5.5.17',
            'version' => '5.5.17.0',
            'reference' => 'f8253982d1adf7200218cbb49f4364db047b235c',
            'type' => 'grav-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'scssphp/scssphp' => array(
            'pretty_version' => 'v1.11.1',
            'version' => '1.11.1.0',
            'reference' => 'ace2503684bab0dcc817d7614c8a54b865122414',
            'type' => 'library',
            'install_path' => __DIR__ . '/../scssphp/scssphp',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
